package Walmart;

public class Tires {

	public String modelNumber;
	public String manufacture;
	
	
	
	public Tires(String modelNumber, String manufacture) {
		this.modelNumber = modelNumber;
		this.manufacture = manufacture;
	}
	public String getModelNumber() {
		return modelNumber;
	}
	public void setModelNumber(String modelNumber) {
		this.modelNumber = modelNumber;
	}
	public String getManufacture() {
		return manufacture;
	}
	public void setManufacture(String manufacture) {
		this.manufacture = manufacture;
	}
	@Override
	public String toString() {
		return "Tires [modelNumber=" + modelNumber + ", manufacture=" + manufacture + "]";
	}

}
