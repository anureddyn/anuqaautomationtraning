package Walmart;

import java.io.File;
import java.io.IOException;

public class CreateNewFile {

	public static void main(String[] args) {
		try {
			
		File obj = new File(".\\files\\RichmanCars.txt");
		 
			if(obj.createNewFile()) {
				System.out.println("File created: "  +obj.getName());
				System.out.println("Absolute Path: "  +obj.getAbsolutePath());
				
			}else {
				System.out.println("File already exists.");
			}
		} catch(IOException e) {
			System.out.println("An error occurred");
			e.printStackTrace();
		}
		
		
		
	}
	
	}
