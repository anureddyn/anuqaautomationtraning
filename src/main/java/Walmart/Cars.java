package Walmart;

import java.util.Date;

public abstract class Cars {

	private Engine engine;
	private Tires tires;
	private int Doors;
	private boolean hasAndroidAuto;
	private boolean hasCarPlay;
	private String Vin;
	private Date LastOilChangeDate;
	private Date LastTireChangeDate;
	private String colors;
	
	public Cars() {
	
	}

	public Cars(Engine engine, Tires tires, int doors, boolean hasAndroidAuto, boolean hasCarPlay, String vin,
			Date lastOilChangeDate, Date lastTireChangeDate, String colors) {
		super();
		this.engine = engine;
		this.tires = tires;
		Doors = doors;
		this.hasAndroidAuto = hasAndroidAuto;
		this.hasCarPlay = hasCarPlay;
		Vin = vin;
		LastOilChangeDate = lastOilChangeDate;
		LastTireChangeDate = lastTireChangeDate;
		this.colors = colors;
	}

	public Engine getEngine() {
		return engine;
	}

	public void setEngine(Engine engine) {
		this.engine = engine;
	}

	public Tires getTires() {
		return tires;
	}

	public void setTires(Tires tires) {
		this.tires = tires;
	}

	public int getDoors() {
		return Doors;
	}

	public void setDoors(int doors) {
		Doors = doors;
	}

	public boolean isHasAndroidAuto() {
		return hasAndroidAuto;
	}

	public void setHasAndroidAuto(boolean hasAndroidAuto) {
		this.hasAndroidAuto = hasAndroidAuto;
	}

	public boolean isHasCarPlay() {
		return hasCarPlay;
	}

	public void setHasCarPlay(boolean hasCarPlay) {
		this.hasCarPlay = hasCarPlay;
	}

	public String getVin() {
		return Vin;
	}

	public void setVin(String vin) {
		Vin = vin;
	}

	public Date getLastOilChangeDate() {
		return LastOilChangeDate;
	}

	public void setLastOilChangeDate(Date lastOilChangeDate) {
		this.LastOilChangeDate = lastOilChangeDate ;
	}

	public Date getLastTireChangeDate() {
		return LastTireChangeDate;
	}

	public void setLastTireChangeDate(Date lastTireChangeDate) {
		this.LastTireChangeDate = lastTireChangeDate;
	}

	public String getColors() {
		return colors;
	}

	public void setColors(String colors) {
		this.colors = colors;
	}

	@Override
	public String toString() {
		return "Cars [engine=" + engine + ", tires=" + tires + ", Doors=" + Doors + ", hasAndroidAuto=" + hasAndroidAuto
				+ ", hasCarPlay=" + hasCarPlay + ", Vin=" + Vin + ", LastOilChangeDate=" + LastOilChangeDate
				+ ", LastTireChangeDate=" + LastTireChangeDate + ", colors=" + colors + "]";
	}

	


	
	

}