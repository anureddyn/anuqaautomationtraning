package Walmart;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RichMan {

	// public static void main(String[] args) {
	List<Cars> mycars = new ArrayList<>();

	public void setMyCars(List<Cars> myCars) {
		this.mycars = myCars;

	}

	public List<Cars> getMyCars() {
		return mycars;
	}

	Scanner in = new Scanner(System.in);
	Scanner userInput = new Scanner(System.in);
	Revenue r = new Revenue();

	public void displayMenu() {
		String choice;

		boolean isExit = false;
		while (isExit == false) {

			System.out.println("Choose one of the option");
			System.out.println("Press A to Add a New Car");
			System.out.println("Press B Modify Car Features");
			System.out.println("Press C Repaint");
			System.out.println("Press D to Perform Regular Services");
			System.out.println("Press E Exit");
			choice = in.nextLine().toUpperCase();
			switch (choice) {
			case "A":
				System.out.println("Enter car name from list: "
						+ "Camero, Challenger, Mustang, Coupe, Sedan, Hatchback, Veloster, Spark");
				try {
					String CarType = in.nextLine();
					Cars newCar = CarFactory.getCar(CarType);
					System.out.println("Enter Vin Number for new car");
					newCar.setVin(in.nextLine());
					//System.out.println(newCar.getVin());
					System.out.println("Now you added a car");
					
				this.addCar(newCar);
				} catch (Exception exc) {
					System.out.println(
							// exc.printStackTrace(),
							exc.getMessage());

				}

				break;

			case "B":
				System.out.println("Enter your VIN number to modify car features:");
				
				try {
					this.modify(CarFactory.getCarByVinNumber(in.nextLine(), mycars));
					System.out.println("Your VIN is matched");
					//System.out.println("");
				
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				break;

			case "C":
				System.out.println("Enter vin to modify");
				String vin = in.nextLine();

				Cars Repaint;
				try {
					Repaint = CarFactory.getCarByVinNumber(vin, mycars);
					System.out.println("Enter new color");
					String newColor = in.nextLine();
					this.repaint(Repaint, newColor);
				} catch (Exception e) {
					System.out.println("Error occured while recoloring the car");
				}
				break;

			case "D":
				System.out.println("Enter vin to modify");
				String vinNo = in.nextLine();
				try {
					Cars carToService = CarFactory.getCarByVinNumber(vinNo, mycars);
					System.out.println("Enter 1 for oil chnage or 2 for tire change");
					int oilOrTireChange = in.nextInt();
					this.performService(carToService, oilOrTireChange);
				} catch (Exception e) {
					System.out.println("Error occured while servicing your car" + e.getMessage());
				}
				break;

			case "E":
				isExit = true;
				break;
			}

		}
	}

	public void addCar(Cars c) {
		this.mycars.add(c);
	}

	public void modify(Cars c) {

	}

	public void repaint(Cars c, String newColor) {
		c.setColors(newColor);
	}

	public void performService(Cars c, int oilOrTireChange) {
		if (oilOrTireChange == 1) {
			r.oilChange(c);
			System.out.println(c.getLastOilChangeDate());
		} else {
			r.tireChange(c);
			System.out.println(c.getLastTireChangeDate());
		}
	}

	public static void main(String[] args) {
		RichMan man = new RichMan();
		man.displayMenu();
	}

}
