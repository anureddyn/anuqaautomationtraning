package Walmart;

public class Engine {
	private String model;
	private String type;
	private String oilchange;

	public Engine(String model, String type, String oilchange) {
		this.model = model;
		this.type = type;
		this.oilchange = oilchange;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOilchange() {
		return oilchange;
	}

	public void setOilchange(String oilchange) {
		this.oilchange = oilchange;
	}

	@Override
	public String toString() {
		return "Engine [model=" + model + ", type=" + type + ", oilchange=" + oilchange + "]";
	}

}
