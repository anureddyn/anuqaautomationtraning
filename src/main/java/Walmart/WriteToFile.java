package Walmart;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
//import java.util.StringTokenizer;

public class WriteToFile {

	public static void main(String[] args) {

		Engine engine = new Engine("New Engine", "V8", "Yes");
		Tires tire = new Tires("Some tire Model number", "Manufacture date here");

		//Cars car = new Cars(engine, tire, 4, true, true, "35546541654165",
		//"12/02/02", "12/02/15", "Blue");

		Cars car = new Cars() {
		};
		car.setEngine(engine);
		car.setTires(tire);
		car.setDoors(4);
		car.setHasAndroidAuto(true);
		car.setHasCarPlay(true);
		car.setVin("1241245415");
		car.getLastOilChangeDate();
		car.getLastTireChangeDate();
		car.setColors("Blue");

		List<Cars> carList = new ArrayList<Cars>(20);
		carList.add(car);

		try {
			// listOfCars.add()
			FileWriter myWriter = new FileWriter(".\\files\\RichmanCars.txt");

			for (int i = 0; i < 20; i++) {
				carList.add(i, car);

				myWriter.write(carList.get(i) + System.lineSeparator());

			}

			myWriter.close();
			System.out.println("Successfully wrote to the file.");

		} catch (IOException e) {
			System.out.println("An error occured.");
			e.printStackTrace();
		}
	}

}
