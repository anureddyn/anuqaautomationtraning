package com.example.Reverse;

public class ReverseString {
      public static void main(String[] args) {
		
    	  String a = "GREEN";
    	  System.out.println("String"  +  a);
    	  
    	  //get length of string 
    	  int length = a.length();
    	  System.out.print("Reversed string is:");
    	  for (int i = length -1; i>=0; --i) {
    		  System.out.println(a.charAt(i));
    	  }
	}
}
