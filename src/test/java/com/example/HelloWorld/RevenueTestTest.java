package com.example.HelloWorld;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import Walmart.Camero;
import Walmart.Cars;
import Walmart.Challenger;
import Walmart.Mustang;
import Walmart.Revenue;
import Walmart.Sedan;
import Walmart.Spark;
import Walmart.Veloster;

public class RevenueTestTest {
	private List<Cars> cars = new ArrayList<Cars>();
	private Revenue revenue = new Revenue();
	
	@Before
	public void setUp() {
	for (int i = 0; i < 20; i++) {
		if (i < 10) {
		cars.add(new Sedan());
		} else if (i < 12) {
		cars.add(new Spark());
		} else if (i < 16) {
		cars.add(new Veloster());
		} else if (i < 18) {
		cars.add(new Mustang());
		} else if (i < 19) {
		cars.add(new Camero());
		} else {
		cars.add(new Challenger());
		}
		}
	}
	
       @Test
	   public void testRevenue() {
		for(Cars car: cars) {
			revenue.oilChange(car);
		}
		Assert.assertEquals(779.7999, revenue.getRevenue(), 0.1);
	}

}
